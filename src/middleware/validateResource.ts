import { Request, Response, NextFunction } from 'express'
import { AnyZodObject } from 'zod'

const validate =
    (schema: AnyZodObject) => 
        (req: Request, res: Response, next: NextFunction) => {
            try {
                console.log("validate")
                

                console.log("Header")
                console.log(req.header)

                console.log("BODY")
                console.log(req.body)
                

                console.log("query")
                console.log(req.query)

                console.log("params")
                console.log(req.params)

                schema.parse({
                    body: req.body,
                    query: req.query,
                    params: req.params
                });
                next();
            } catch (e: any) {
                console.log("BAD request")
                return res.status(400).send(e.errors)
            }
        }

export default validate