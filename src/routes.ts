import { Express, Request, Response } from 'express'
import { AddProductHandler, deleteProductHandler, getProductHandler, updateProductHandler, getAllProductHandler } from './controller/product.controller'
import validate from './middleware/validateResource'
import { AddUserSchema, AddUserSchema2, deleteProductSchema, getProductSchema, updateProductSchema } from './schema/product.schema'
function routes(app: Express) {
    try {
        app.get('/productcheck', (req: Request, res: Response) => res.sendStatus(200))
        app.get("/api/search/:productno", (req: Request, res: Response) => res.send(req.params))
        app.get("/api/search2/", (req: Request, res: Response) => res.send(req.query))




        app.get("/api/products/",  getAllProductHandler)

        app.get("/api/products/:_id?", validate(getProductSchema), getProductHandler)
        app.post("/api/product", validate(AddUserSchema2), AddProductHandler)
        app.put("/api/products/:_id?", validate(updateProductSchema), updateProductHandler)       
        app.delete("/api/products/:_id", validate(deleteProductSchema), deleteProductHandler)



    } catch (error) {
        console.log(error)
    }
}

export default routes