import express from "express";
import config from 'config'
import connect from './utils/connect'
import logger from './utils/logger'
import routes from "./routes";
import cors from 'cors';




const port = config.get<number>('port')
const allowedOrigins = ['http://localhost:4200'];
const options: cors.CorsOptions = {
    origin: allowedOrigins
  };

const app = express();

app.use(cors(options));
app.use(express.urlencoded());
app.use(express.json())
app.listen(port, async () => {
    logger.info(`App is running at http://localhost:${port}`)

    await connect();

    routes(app)
});
