import { DocumentDefinition, FilterQuery, QueryOptions, UpdateQuery } from 'mongoose'
import ProductModel, { ProductDocument } from '../models/product.model'


export async function AddProduct(
    input: DocumentDefinition<Omit<ProductDocument,  'ProductId' | 'createAt' | 'updateAt'>>
) {
    try {
        return await ProductModel.create(input)
    } catch (error: any) {
        throw new Error(error);

    }
}

export async function FindProduct(
    query: FilterQuery<ProductDocument>,
    options: QueryOptions = { lean: true }

) {
    console.log("Query ::" , query)
    return ProductModel.findOne(query, {}, options)
}

export async function FindAndUpdateProduct(
    query: FilterQuery<Omit<ProductDocument, 'ProductPrice' | 'ProductQuantity'>>,
    update: UpdateQuery<ProductDocument>,
    options: QueryOptions
) {
    return ProductModel.findOneAndUpdate(query, update, options)
}

export async function deleteProduct(
    query: FilterQuery<ProductDocument>
    

) {
    return ProductModel.deleteOne(query)
}



export async function FindAllProduct() {    
    return ProductModel.find()
}
