import { number, object, string, TypeOf } from "zod";



export const AddUserSchema = object({
    body: object({
        ProductNo: number({
            required_error: 'ProductNo is required'
        }),
        ProductTitle: string({
            required_error: 'Prodcut title is required'
        }),
        ProductPrice: number({
            required_error: 'ProductPrice is required'
        }),
        ProductQuantity: number({
            required_error: 'ProductQuantity is required'

        })
    })
})

const Userpayload = ({
    body: object({
        
        ProductTitle: string({
            required_error: 'Prodcut title is required'
        }),
        ProductPrice: number({
            required_error: 'ProductPrice is required'
        }),
        ProductQuantity: number({
            required_error: 'ProductQuantity is required'

        })
    })

})



const payload = {
    body: object({
        //ProductNo: number({ required_error: "Product is Required" }),
        ProductTitle: string({ required_error: "ProductTitle is Required" }),
        ProductPrice: number({ required_error: "ProductPrice is Required" }),
        ProductQuantity: number({ required_error: "ProductQuantity is Required" }),
    })
}

const params = {
    params: object({
        _id: string({
            required_error: "_id is Required !!!"
        })
    })
}

export const createProductSchema = object({
    ...payload
})

export const updateProductSchema = object({
    ...payload,
    ...params
})

export const getProductSchema = object({
    ...params
})
export const deleteProductSchema = object({
    ...params
})


export const AddUserSchema2 = object(Userpayload)


console.log("Type of : ",typeof AddUserSchema)
type testType = TypeOf<typeof createProductSchema>

type OmitA = Omit<typeof payload, 'ProductNo'>

export type AddProductInput = TypeOf<typeof AddUserSchema>
export type createProductInput = TypeOf<typeof createProductSchema>
export type updateProductInput = TypeOf<typeof updateProductSchema>
export type ReadProductInput = TypeOf<typeof getProductSchema>
export type DeleteProductInput = TypeOf<typeof deleteProductSchema>
