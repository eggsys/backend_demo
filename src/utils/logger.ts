import logger from 'pino'
import dayjs, { Dayjs } from 'dayjs'

const log = logger({
    prettyPrint: true,
    base: {
        pid: false
    },
    timestamp: () => `,"Time":"${dayjs().format()}" `
})


export default log