import mongoose from 'mongoose'
import config from 'config'
import logger from './logger'
async function connect() {
    const dbUri = config.get<string>("dbUri")

    try {
        await mongoose.connect(dbUri)
        logger.info(" Mongoose -- Mongo Connected")
    } catch (error) {
        logger.error("Could not connecto to DB \n", error)
        process.exit(1)
    }
}


export default connect