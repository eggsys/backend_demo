import mongoose, { Document } from 'mongoose'
import config from 'config'
import { number } from 'zod'
import { customAlphabet } from 'nanoid'

const nanoid = customAlphabet('abcdefghijklmnopqrstuvwxyz0123456789', 10)

export interface ProductDocument extends mongoose.Document {
    ProductId: String,
    ProductNo: Number
    ProductTitle: String
    ProductPrice: Number
    ProductQuantity: Number
    createAt: Date
    updateAt: Date
}

const productSchema = new mongoose.Schema({
    ProductId: { type: String, required: true, unique: true, default: () => `product_${nanoid()}` },
    ProductNo: { type: Number },
    ProductTitle: { type: String, required: true },
    ProductPrice: { type: Number, required: true },
    ProductQuantity: { type: Number, required: true },


}, {
    timestamps: true
})

productSchema.pre("save", async function (next) {

    try {
        let user = this as ProductDocument
        return next()

    } catch (error: any) {
        console.log("Model Error !")
        console.error(error)
    }


})

const ProductModel = mongoose.model("product", productSchema)

export default ProductModel