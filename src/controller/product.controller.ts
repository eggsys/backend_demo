import { Request, Response } from 'express'
import { rearg } from 'lodash'
import { send } from 'process'
import { AddProductInput, createProductInput, updateProductInput, ReadProductInput, DeleteProductInput } from '../schema/product.schema'
import { AddProduct, FindProduct, FindAndUpdateProduct, deleteProduct, FindAllProduct } from '../service/product.service'
import logger from '../utils/logger'


export async function AddProductHandler(
    req: Request<{}, {}, AddProductInput["body"]>,
    res: Response) {
    try {
        // const product = await // call product create service

        console.log(req.body)
        const product = await AddProduct(req.body)
        return res.send(product)
    } catch (error) {
        logger.error(error)
        return res.status(409).send(error)

    }
}

export async function createProductHandler(
    req: Request<{}, {}, createProductInput["body"]>,
    res: Response
) {

}

export async function getProductHandler(
    req: Request<ReadProductInput["params"]>,
    res: Response) {
    console.log("GET ProductHandler")
    //const ProductNo = Number(req.params.ProductNo);
    const _id = req.params._id



    //let ProductNo: number = +ProductNos
    console.log(" _id = ", _id)
    const product = await FindProduct({ _id })

    if (!product) {
        return res.sendStatus(404)
    }
    return res.send(product)

}

export async function getAllProductHandler(
    req: Request<ReadProductInput["params"]>,
    res: Response) {
    console.log("getAllProductHandler")

    const product = await FindAllProduct()

    if (!product) {
        return res.sendStatus(404)
    }
    return res.send(product)

}


export async function updateProductHandler(
    req: Request<Omit<updateProductInput["params"], 'ProductPrice'>>,
    res: Response) {

    console.log("UPDATE Handler")
    const _id = req.params._id
    const update = req.body

    console.log(_id)
    console.log(update)
    const product = await FindProduct({ _id })
    if (!product) {
        return res.sendStatus(404)
    }

    const updateProduct = await FindAndUpdateProduct({ _id }, update, {
        new: true,
    })

    return res.send(updateProduct)
}

export async function deleteProductHandler(
    req: Request<DeleteProductInput["params"]>,
    res: Response) {

    const _id = req.params._id
    const product = await FindProduct({ _id })
    if (!product) {
        return res.sendStatus(404)
    }
    console.log(product)

    console.log("---> ", _id)

    await deleteProduct({ _id })

    //return res.sendStatus(200)
    return res.send(product)


}